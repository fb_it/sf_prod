import { LightningElement, wire, api, track } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import { refreshApex } from '@salesforce/apex';
import PAPA from '@salesforce/resourceUrl/papaparse';

import DATE_FIELD from '@salesforce/schema/AcctSeedERP__Purchase_Order_Line__History.CreatedDate';
import POLNAME_FIELD from '@salesforce/schema/AcctSeedERP__Purchase_Order_Line__c.Name';
import POLID_FIELD from '@salesforce/schema/AcctSeedERP__Purchase_Order_Line__c.Id';
import PRODUCT_FIELD from '@salesforce/schema/AcctSeedERP__Purchase_Order_Line__c.AcctSeedERP__Product__r.Name';
import PRODUCTID_FIELD from '@salesforce/schema/AcctSeedERP__Purchase_Order_Line__c.AcctSeedERP__Product__r.Id';
import PRODUCTSKU_FIELD from '@salesforce/schema/AcctSeedERP__Purchase_Order_Line__c.AcctSeedERP__Product__r.StockKeepingUnit';
import CREATEDBY_FIELD from '@salesforce/schema/User.Alias';
import FIELD_FIELD from '@salesforce/schema/AcctSeedERP__Purchase_Order_Line__History.Field';
import OLDVALUE_FIELD from '@salesforce/schema/AcctSeedERP__Purchase_Order_Line__History.OldValue';
import NEWVALUE_FIELD from '@salesforce/schema/AcctSeedERP__Purchase_Order_Line__History.NewValue';

import getPoPolHistory from '@salesforce/apex/PurchaseOrderLineHistoryController.getPoPolHistory';
//TODO: Include/ sort by Modified date?
//TODO: Include user field
const COLUMNS = [
    
    { 
        label: 'Date',
        fieldName: DATE_FIELD.fieldApiName,
        type: 'date',
        typeAttributes:{
            year: "numeric",
            month: "2-digit",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit"
        },
         },
    { 
        label: 'POL Name',
        fieldName: POLID_FIELD.fieldApiName,
        type:'url',
        typeAttributes: {
            label: { fieldName: POLNAME_FIELD.fieldApiName }, 
            value: { fieldName: POLID_FIELD.fieldApiName },
            target: '_blank'
          },
    },
    { 
        label: 'Product Name',
        fieldName: PRODUCTID_FIELD.fieldApiName,
        type:'url',
        typeAttributes: {
            label: { fieldName: PRODUCT_FIELD.fieldApiName }, 
            value: { fieldName: PRODUCTID_FIELD.fieldApiName },
            target: '_blank'
          },        
    },
    { label: 'Product SKU', fieldName: PRODUCTSKU_FIELD.fieldApiName, type: 'text' },
    { 
        label: 'User',
        fieldName: CREATEDBY_FIELD.fieldApiName,
        type:'text',
        },        
    { label: 'Changed Field', fieldName: FIELD_FIELD.fieldApiName, type: 'text' },
    { label: 'Old Value', fieldName: OLDVALUE_FIELD.fieldApiName, type: 'text' },
    { label: 'New Value', fieldName: NEWVALUE_FIELD.fieldApiName, type: 'text'}

    
];
export default class poPolHistory extends LightningElement {

    @track poPolHistory;
    @track data;
    @track error;
    @api recordId;
    @track hrefdata;
    @track sortBy;
    @track sortDirection;
    columns = COLUMNS;
    @track polPoResultsValue;
    @track polPoResults;
    // csv;
    @track csvhref='data:text/csv;charset=utf-8,';



    @wire(getPoPolHistory, { recordId: '$recordId' })
    poPolResults(value){
        this.polPoResultsValue=value;
        const {data, error}=value;
        if (data) {
            // console.log(data);
            this.poPolHistory= data.map(row=>{
                return{
                    ...row,
                    //TODO: Make these links
                    [POLNAME_FIELD.fieldApiName]: row.Parent.Name,
                    [POLID_FIELD.fieldApiName]: `/${row.Parent.Id}`,
                    [PRODUCT_FIELD.fieldApiName]: row.Parent.AcctSeedERP__Product__r.Name,
                    [PRODUCTID_FIELD.fieldApiName]: `/${row.Parent.AcctSeedERP__Product__r.Id}`,
                    [PRODUCTSKU_FIELD.fieldApiName]: row.Parent.AcctSeedERP__Product__r.StockKeepingUnit,
                    [CREATEDBY_FIELD.fieldApiName]: row.CreatedBy.Alias
                }
            })
            this.polPoResults=this.poPolHistory;
            this.error = undefined;
        }
        else if (error){
            this.poPolHistory=undefined;
            this.error=error;
            console.log('This errored out with '+ error );
        }
    }
    ready = false;
    connectedCallback() {
        loadScript(this, PAPA)
        .then(()=>{
            this.ready=true;
        });
    }



    downloadCSVFile() {   
        console.log('clickity!');
        console.log('pprv');
        console.log(this.polPoResultsValue)
        console.log('pph');
        console.log(JSON.parse(JSON.stringify(this.poPolHistory)));
        
        var csv= Papa.unparse(this.poPolHistory, {quotes:true, quoteChar: '"',escapeFormulae:true});
        console.log('csv '+csv);
        this.csvhref = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
        let downloadElement = document.getElementById('csvDownload');
         downloadElement.href = csvhref;
         downloadElement.click(); 
    }

    refreshApexHandler() {
        return refreshApex(this.polPoResultsValue);
      }
    

}
//SOQL data string for tsalb's thing: 
//TODO: Add "Load more data"?
    /*
    loadMoreStatus;
    @api totalNumberOfRows;

    loadMoreData(event) {
            //Display a spinner to signal that data is being loaded
            event.target.isLoading = true;
            //Display "Loading" when more data is being loaded
            this.loadMoreStatus = 'Loading';
            this.counter= counter+20;
            getPoPolHistory( '$recordId', counter )
                .then((data) => {
                    if (data.length >= this.totalNumberOfRows) {
                        event.target.enableInfiniteLoading = false;
                        this.loadMoreStatus = 'No more data to load';
                    } else {
                        const currentData = this.data;
                        //Appends new data to the end of the table
                        const newData = currentData.concat(poPolHistory);
                        this.data = newData;
                        this.loadMoreStatus = '';
                    }
                    event.target.isLoading = false;
                });
        }
hypothetical sorting code:
   handleSortdata(event) {
        // field name
        this.sortBy = event.detail.fieldName;

        // sort direction
        this.sortDirection = event.detail.sortDirection;

        // calling sortdata function to sort the data based on direction and selected field
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
    }

    sortData(fieldname, direction) {
        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.poPolHistory));

        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction 
        let isReverse = direction === 'asc' ? 1: -1;

        // sorting data 
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        this.poPolHistory = parseData;

    }
        */
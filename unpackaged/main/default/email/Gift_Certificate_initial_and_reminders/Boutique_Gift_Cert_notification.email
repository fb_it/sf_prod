<messaging:emailTemplate subject="Your Gift Certificate Award Letter from {!relatedTo.Sender_Name__c} & First Book!" recipientType="Contact" relatedToType="npsp__Allocation__c">

<messaging:htmlEmailBody >
<style>
         .sfdc_richtext img{
                border: 0;
                height: 300px;
                width: 300px;
                
            }
</style>
<apex:outputField value="{!relatedTo.npsp__Campaign__r.Gift_Cert_custom_email_section__c}" />

<p><div><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">Dear {!relatedTo.Recipient_Name__c},<br />
<br />
Congratulations! Thanks to a donation from {!relatedTo.Sender_Name__c} you have been awarded a gift certificate with a balance of <span style="background-color:#FFFF00;">$<apex:outputText value="{0,number,00.00}"><apex:param value="{!relatedTo.Remaining_Balance__c}"/></apex:outputText></span> for use on the First Book Marketplace.&nbsp;{!relatedTo.Gift_Cert_Condition_Sentence__c}<br />
<br />
<span style="text-align:center"><b>{!relatedTo.Magento_URL_custom_or_standard__c}</b></span>
<br />
<br />
<strong>Gift certificate&nbsp;details:</strong></span></span></div>

<ul>
    <li><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">Your gift certificate code is: <span style="background-color:#FFFF00;">{!relatedTo.Cert_Number__c}</span></span></span></li>
    <li><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">These funds will only be available through {!month(relatedTo.Expire_At__c)}/{!day(relatedTo.Expire_At__c)}/{!year(relatedTo.Expire_At__c)}.</span></span></li>
    <li><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">This gift certificate&nbsp;is unique to you. Do not share your code.</span></span></li>
</ul>
<span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">{!relatedTo.Recipient_Message__c}</span></span>
<br />
<br />
<div><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><strong>How to redeem your gift certificate:</strong></span></span></div>

<ul>
    <li><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><a href="https://www.fbmarketplace.org/customer/account/login/">Log in to your First Book Marketplace account</a> by clicking the &quot;Log In To Shop&quot; button at the top right hand corner of your screen. If you do not have an account set up yet, <a href="http://www.fbmarketplace.org/register" style="color:blue;">click here to sign up&nbsp;now</a>.</span></span></li>
    <li><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">Add books and/or resources to your shopping cart. When adding items to your cart, please be mindful of any restrictions to specific items or categories noted above.</span></span></li>
    <li><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">Copy and paste the code above in the field titled &quot;Gift Certificates&rdquo; and click the button that says &ldquo;Add Gift Certificate&rdquo;.</span></span></li>
    <li><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">You will see your order subtotal update automatically.*</span></span></li>
    <li><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">Please note that you are not required to spend the full balance of your Gift Certificate on one order; you are welcome to place multiple orders with your available funding if that is easier to manage!</span></span></li>
</ul>

<div><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><i>*If your gift certificate&nbsp;does not cover the full amount of your order, you will need to provide a payment method to cover the balance. If the gift certificate does cover the entire balance of your order, you will know that your gift certificate has been successfully applied because at checkout, you will see a payment method option that says &ldquo;No Payment Information Required.&rdquo;</i><br />
&nbsp;<br /></span></span></div>

<span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><strong>Information on ordering through the First Book Marketplace:</strong></span></span>

<ul>
    <li><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">Your order will arrive 3 to 14 business days (depending on geography) from the date full payment is received. Please be mindful of this time frame when selecting your shipping address.**</span></span></li>
    <li><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">Your order may arrive in separate shipments or all at once, and tracking information will be emailed to you once your items ship.</span></span></li>
    <li><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">If you would like to expedite your order, please give us a call at 866-732-3669 within 2 hours or placing your order. Please note that there is an additional fee for expedited shipping.&nbsp;</span></span></li>
</ul>

<div><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><i>**This standard shipping time frame does not apply to certain items that ship from third party vendors. The shipping time frame for these items will be indicated on the product page, and unfortunately they cannot be expedited.&nbsp;</i><br />
&nbsp;<br />
If you have any questions about the funding available to your school or program to purchase books and/or resources on the First Book Marketplace please feel free to contact our Member Services team at 866-READ-NOW or <a href="mailto:help@firstbook.org" style="color:blue;">help@firstbook.org</a>.<br />
&nbsp;<br />
Thank you and happy reading!<br />
<br />
Your friends at First Book</span></span></div>
</p>
 
</messaging:htmlEmailBody>
</messaging:emailTemplate>
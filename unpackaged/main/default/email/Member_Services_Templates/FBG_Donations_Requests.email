<table border="0" cellpadding="5" width="550" cellspacing="5" height="400" >
<tr valign="top" height="400" >
<td tEditID="c1r1" style=" background-color:#FFFFFF; color:#000000; bEditID:r3st1; bLabel:main; font-size:12pt; font-family:arial;" aEditID="c1r1" locked="0" >
<![CDATA[<p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 12pt; line-height: normal;"><font style="" size="2" face="Arial, sans-serif"><font size="2">Dear NAME,</font></font></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 12pt; line-height: normal;"><font style="" size="2" face="Arial, sans-serif">
Thank you for contacting First Book</font><span style="font-family: Arial, sans-serif; font-size: 10pt;">! First Book works to get new books
to children from low-income families in the US, Canada and around the world
thanks to our partnerships.</span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 12pt; line-height: normal;"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;">While we are able to offer
high-quality books at 50-90% off retail price through our Global Marketplace, <b>please note that our minimum global
shipping costs begin at $300</b>. This does not include the cost of books.
Navigating customs clearance and taxes may also result in additional fees. <o:p></o:p></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 12pt; line-height: normal;"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;">However, if your organization has an
office or partner organization in the Unities States, then you may be eligible
to sign up with us through our US registration site (</span><a href="http://www.fbmarketplace.org/register"><span style="font-size:10.0pt;
font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">www.fbmarketplace.org/register</span></a><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
&quot;Times New Roman&quot;">) and order books from the US First Book Marketplace (</span><a href="http://www.fbmarketplace.org/"><span style="font-size:10.0pt;font-family:
&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">www.fbmarketplace.org</span></a><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
&quot;Times New Roman&quot;">). Our US Marketplace has over 4,000 high-quality, educational
resources – still at 50-90% off retail – and shipping is free for orders over
$25. <o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 12pt; line-height: normal;"><font face="Arial, sans-serif"><span style="font-size: 13.3333px;">Below is some information about who we are and how our U.S. Marketplace works that may be helpful to you if you have a connection in the United States:</span></font></p><p class="MsoNormal" style="margin-bottom: 12pt; line-height: normal;"><font face="Arial, sans-serif"><span style="font-size: 13.3333px;">First Book is a nonprofit social enterprise that provides educators with the brand new, high-quality books, educational resources, and other essentials needed to help kids learn. First Book has distributed more than 170 million books and educational resources to programs and schools serving children from low income families throughout the United States, Canada and abroad.</span></font></p><p class="MsoNormal" style="margin-bottom: 12pt; line-height: normal;"><font face="Arial, sans-serif"><span style="font-size: 13.3333px;">To access First Book’s free and low-cost resources, you must register your program at <a href="https://www.fbmarketplace.org/register/">http://www.fbmarketplace.org/register/</a>. Registering with First Book is free and is open to anyone working in a Title I eligible school, or schools or programs where at least 70% of the students come from low-income families. The First Book Network includes educators, paraprofessionals, librarians, community program leaders and many other professionals.</span></font></p><p class="MsoNormal" style="margin-bottom: 12pt; line-height: normal;"><font face="Arial, sans-serif"><span style="font-size: 13.3333px;">Once you’ve signed up, you will have immediate access to the First Book Marketplace, our eCommerce site where educators and program leaders serving kids in need can access thousands of affordable, high-quality books and educational resources for kids ages 0-18. In addition to books, we offer a variety of non-book resources such as school supplies, digital learning devices, non-perishable food, basic needs, and clothing items. You will also have access to free resources for educators, such as tip sheets to support student learning and development, webinar recordings on critical topics, and other online learning tools available to First Book members at no cost.</span></font></p><p class="MsoNormal" style="margin-bottom: 12pt; line-height: normal;"><font face="Arial, sans-serif"><span style="font-size: 13.3333px;">First Book members can also access brand new books and educational resources by the carton from the Book Bank section of the First Book Marketplace for only the cost of shipping &amp; handling—the books are FREE! Book Bank items were generously donated by our publishing partners and are ideal for members with limited budgets, high quantity needs, and greater flexibility around content.</span></font></p><p class="MsoNormal" style="margin-bottom: 12pt; line-height: normal;"><font face="Arial, sans-serif"><span style="font-size: 13.3333px;">In addition to these online resources, we work with numerous corporate partners and generous donors to subsidize the cost of books and resources for our members. We frequently release new offers that allow you to further increase the impact of your dollars by taking advantage of additional discounts or free offerings throughout the First Book Marketplace. These opportunities will be sent directly to you via email when funding is available, so keep an eye on future e-mail communications from First Book once you register with us.</span></font></p><p class="MsoNormal" style="margin-bottom: 12pt; line-height: normal;"></p><p class="MsoNormal" style="margin-bottom: 12pt; line-height: normal;"><font face="Arial, sans-serif"><span style="font-size: 13.3333px;">Please note that First Book does not distribute books or resources that are not specifically ordered by our members. Once you’re ready to receive books, it’s up to you to take advantage of the resources listed above.</span></font></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 12pt; line-height: normal;"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;">If you do not have an office or
connection in the United States, then you can register by visiting </span><a href="http://www.firstbookglobal.org/register"><span style="font-size:10.0pt;
font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;">www.firstbookglobal.org/register</span></a><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
&quot;Times New Roman&quot;">.</span><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
&quot;Times New Roman&quot;"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;"> </span>Please note that you will need to provide us with one of
the following when you register:<o:p></o:p></span></p><p class="MsoListParagraphCxSpFirst" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 12pt; text-indent: -0.25in; line-height: normal;"></p><ul style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><li><span style="font-size:10.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol"><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;"></span></span><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;">Proof of partnership with a
reputable international organization (eg: USAID, UN, WHO, IDB)<o:p></o:p></span></li><li><span style="font-size:10.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol"><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;"></span></span><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;">Proof on non-profit status from your
organization’s home government<o:p></o:p></span></li></ul><!--[if !supportLists]--><p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 12pt; line-height: normal;">











</p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 12pt; line-height: normal;"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;">Email your documents to help@firstbook.org. If you have uploaded or mailed your documents by postal mail, please email a copy of your documents and notify us of your intent to register. </span><o:p></o:p></p>

<p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 0.0001pt; line-height: normal;"><font size="2" face="Arial, sans-serif">If you have any questions or concerns,
please reply back to this email or give us a call at
866-732-3669 between 9:00am-5:00pm EST. Please refer to case
#{!Case.CaseNumber} so that we can reference your request when you call.<o:p></o:p></font></p>

<p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 12pt; line-height: normal;"><span style="font-family: Arial, sans-serif; font-size: small;"><br></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 12pt; line-height: normal;"><span style="font-family: Arial, sans-serif; font-size: small;">All the best,</span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="font-family: Arial, sans-serif; font-size: small;">First Book Member Services Team</span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><br><span style="font-size: small; background-color: rgb(255, 255, 255);"><span style="widows: auto; font-size: small;"><span style="font-size:9.0pt;line-height:115%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;color:black;
background:white;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:
AR-SA">Your Case Information:<span class="apple-converted-space">&nbsp;</span></span><span style="font-size:9.0pt;line-height:115%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;color:black;
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA"><br>
<br>
<span style="background:white">Case #: {!Case.CaseNumber}</span><br>
<span style="background:white">Submitted: {!Case.CreatedDate}<br><br><span class="apple-converted-space">&nbsp;{!Case.Thread_Id}</span></span></span></span></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-bottom: 12pt; line-height: normal;"><span style="font-family: Arial, sans-serif; font-size: small;"></span></p>]]></td>
</tr>
</table>
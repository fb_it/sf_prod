<table border="0" cellpadding="5" width="550" cellspacing="5" height="400" >
<tr valign="top" height="400" >
<td tEditID="c1r1" style=" background-color:#FFFFFF; color:#000000; bEditID:r3st1; bLabel:main; font-size:12pt; font-family:arial;" aEditID="c1r1" locked="0" >
<![CDATA[<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"></p><p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><font style="" size="3" face="Arial,Sans-Serif">Hello </font><font style="" size="2"><font size="3" face="Arial,Sans-Serif">{!Case.Contac<span style="background-color: rgb(255, 255, 255);">t}</span></font><font size="3" face="Arial,Sans-Serif">,</font><br></font><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="background:yellow;
mso-highlight:yellow"></span> <br>
Thank you again for reaching out! We think your inquiry would benefit from a
conversation with our <b>Nonprofit Partner</b>
team. They are reviewing your inquiry now, and will reach out to you within 7
business days. If you have any questions or would like to alter any of the
information submitted in your original request, please reply directly to this
email, or give us a call at 866-732-3669 and reference case {!Case.CaseNumber}.</span><span style="font-family:&quot;Arial&quot;,sans-serif"><font size="3" face="Arial,Sans-Serif"><span style="background:yellow;mso-highlight:yellow"><span style="widows: auto; font-size: small;"><span style="font-size: 9pt; line-height: 115%; color: black;"><span style="background:white"> </span></span></span></span></font><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><span style="font-family:&quot;Arial&quot;,sans-serif">While you wait to hear from our
Nonprofit Partner friends, we’ve compiled some information below to help you
learn more about First Book and how we serve kids in need.<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;</span></p><p class="MsoListParagraphCxSpFirst" style="margin-bottom:0in;margin-bottom:.0001pt;
mso-add-space:auto;text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><b><span style="font-family:&quot;Arial&quot;,sans-serif">What is First Book?</span></b><span style="font-family:&quot;Arial&quot;,sans-serif"> </span><a href="https://firstbook.org/"><span style="font-family:&quot;Arial&quot;,sans-serif">First Book</span></a><span style="font-family:&quot;Arial&quot;,sans-serif"> is a nonprofit social enterprise that
provides educators with the brand new, high-quality books, educational
resources, and other essentials needed to help kids learn. First Book has
distributed more than 170 million books and educational resources to programs
and schools serving children from low-income families throughout the United
States, Canada and abroad. <o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0in;margin-bottom:
.0001pt;mso-add-space:auto"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;</span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0in;margin-bottom:
.0001pt;mso-add-space:auto;text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><b><span style="font-family:&quot;Arial&quot;,sans-serif">How do I become a First Book member?</span></b><span style="font-family:&quot;Arial&quot;,sans-serif"> Eligible educators, librarians, program
leaders, and others serving children in need can sign up online to become
members of the First Book Network. To learn more and register your school or
program, please visit </span><a href="http://www.fbmarketplace.org/register"><span style="font-family:&quot;Arial&quot;,sans-serif">http://www.fbmarketplace.org/register</span></a><span style="font-family:&quot;Arial&quot;,sans-serif">. <o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0in;margin-bottom:
.0001pt;mso-add-space:auto"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;</span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0in;margin-bottom:
.0001pt;mso-add-space:auto;text-indent:-.25in;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><b><span style="font-family:&quot;Arial&quot;,sans-serif">How do I access First Book’s resources?</span></b><span style="font-family:&quot;Arial&quot;,sans-serif"> All members of the First Book Network
have exclusive, 24/7 access to the First Book Marketplace (</span><a href="http://www.fbmarketplace.org/"><span style="font-family:&quot;Arial&quot;,sans-serif">www.fbmarketplace.org</span></a><span style="font-family:&quot;Arial&quot;,sans-serif">).</span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0in;margin-bottom:
.0001pt;mso-add-space:auto"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;</span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0in;margin-bottom:
.0001pt;mso-add-space:auto"><span style="font-family:&quot;Arial&quot;,sans-serif">On the
First Book Marketplace you can find thousands of <b>brand new resources at 50-90%</b> off retail price for kids from birth
to 18 -&nbsp; including award winning and
culturally inclusive books, school supplies, digital devices, teacher
resources, hygiene kits, and so much more!<o:p></o:p></span></p><p class="MsoListParagraphCxSpMiddle" style="margin-bottom:0in;margin-bottom:
.0001pt;mso-add-space:auto"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;</span></p><p class="MsoListParagraphCxSpLast" style="margin-bottom:0in;margin-bottom:.0001pt;
mso-add-space:auto"><span style="font-family:&quot;Arial&quot;,sans-serif">
<!--[endif]--><o:p></o:p></span></p><p class="MsoListParagraphCxSpLast" style="margin-bottom: 0.0001pt; font-family: &quot;Times New Roman&quot;; font-size: medium;"><font face="Arial, sans-serif">First Book members also have access to brand new books and educational resources by the carton from the&nbsp;<a href="https://www.fbmarketplace.org/book-bank/">Book Bank Section</a>&nbsp;of the First Book Marketplace for only the cost of shipping &amp; handling—the books are FREE! Book Bank items were generously donated by our publishing partners and are ideal for members with limited budgets, high quantity needs, and greater flexibility around content.</font><font face="Arial, sans-serif"><span style="font-size: 12pt;"><o:p></o:p></span></font></p><div><br></div><p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><span style="font-family:&quot;Arial&quot;,sans-serif">
Thank you for your patience as we consider your inquiry, and thank you for your
interest in getting books to kids! <o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><span style="font-family:&quot;Arial&quot;,sans-serif">First Book Member Services Team<o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;</span></p><p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;</span></p><div style="mso-element:para-border-div;border:none;border-bottom:solid windowtext 1.5pt;
padding:0in 0in 1.0pt 0in">

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;border:none;
mso-border-bottom-alt:solid windowtext 1.5pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;</span></p>

</div><p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt">





































</p><p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><span style="font-family:&quot;Arial&quot;,sans-serif">&nbsp;</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><span style="font-family:&quot;Arial&quot;,sans-serif">­­­­­<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><o:p></o:p></span><span style="widows: auto; font-size: small;"><span style="font-size:9.0pt;line-height:115%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;color:black;
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA"><span style="background:white">Case #: {!Case.CaseNumber}</span><br>
<span style="background:white">Submitted: {!Case.CreatedDate}<br><br><span class="apple-converted-space">{!Case.Thread_Id}</span></span></span></span></p><br><p></p>]]></td>
</tr>
</table>
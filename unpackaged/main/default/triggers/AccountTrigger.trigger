/*
Created by Steve Reitz, on February 27, 2020. Sreitz@firstbook.org

*/

trigger AccountTrigger on Account (before insert, before update, after insert, after update) {
    if (trigger.isInsert||trigger.isUpdate) {
        if(trigger.isBefore){
            for ( Account a  : Trigger.New) {
                if(a.Strong_Reporting_Requirements__c==TRUE){
                    AccountTriggerHandler.createGLAV(a.Name);
                }/*
                if(a.Own_PO__c==TRUE){
                    AccountTriggerHandler.clearTLP(a);
                }*/
            }
        
         }
         else {
            for ( Account a  : Trigger.New) {
                if(Trigger.isUpdate){
                    Account aOld=trigger.oldMap.get(a.Id);
                    if(a.Own_PO__c!=aOld.Own_PO__c){
                        AccountTriggerHandler.changePOLevel(a, !a.Own_PO__c);
                    }
                }
                if(Trigger.isInsert){
                    System.debug(a.id);
                    if (a.Own_PO__c==TRUE) {
                        AccountTriggerHandler.changePOLevel(a, FALSE);
                    }
                    
                }
            }
             
         }
    }
}
trigger ProductTrigger on Product2 (before delete) {

    for(Product2 prod: Trigger.old){
        List<AcctSeedERP__Purchase_Order_Line__c> polsWithProd= new List<AcctSeedERP__Purchase_Order_Line__c>();
        polsWithProd=[SELECT Id FROM AcctSeedERP__Purchase_Order_Line__c WHERE 	AcctSeedERP__Product__c=:prod.Id];
        if (polsWithProd.size()>0) {
            prod.addError('APEX ERROR: This Product currently exists on Purchase Order Lines.');
            
        }
    }

}
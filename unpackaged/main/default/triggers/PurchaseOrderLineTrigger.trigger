trigger PurchaseOrderLineTrigger on AcctSeedERP__Purchase_Order_Line__c (after insert, after update, before delete) {
        

        

        if(trigger.isUpdate){
            AcctSeedERP__Purchase_Order_Line__c pol=Trigger.new[0];
            AcctSeedERP__Purchase_Order_Line__c polOld=trigger.oldMap.get(pol.Id);
            String polPOId=pol.AcctSeedERP__Purchase_Order__c;
            PurchaseOrderLineTriggerHandler.updateVersionStatus(pol,polOld, polPOId,FALSE);
        }
        else if(trigger.isDelete){
            AcctSeedERP__Purchase_Order_Line__c polOld=trigger.old[0];
            String polPOId=polOld.AcctSeedERP__Purchase_Order__c;
            PurchaseOrderLineTriggerHandler.updateVersionStatus(polOld,polOld, polPOId,TRUE);
        }
        else if(trigger.isInsert){
            AcctSeedERP__Purchase_Order_Line__c pol=Trigger.new[0];
            String polPOId=pol.AcctSeedERP__Purchase_Order__c;
            PurchaseOrderLineTriggerHandler.setVersionStatus(pol, polPOId);
        }


    //PurchaseOrderLineTriggerHandler.changeVersionStatus(Trigger.new,trigger.old);
}
trigger POIMTrigger on AcctSeedERP__Purchase_Order_Inventory_Movement__c (after insert, after delete) {

    if(trigger.isInsert)
    {
        /*
        POIMTriggerHandler.SendToMagento(JSON.serialize(trigger.new), false);
        POIMTriggerHandler.updateCatalogAvailQty(trigger.new,false);*/
        ID jobId=Database.executeBatch(new InventoryMovementBatchable(Trigger.new,'Purchase Order',false),45);
        System.debug('jobId '+jobId);
    }
    if(trigger.isDelete)
     { 
         //POIMTriggerHandler.SendToMagento(JSON.serialize(trigger.old), true);
         //POIMTriggerHandler.updateCatalogAvailQty(trigger.old,true);
         ID jobId=Database.executeBatch(new InventoryMovementBatchable(Trigger.old,'Purchase Order',true),45);
         System.debug('jobId '+jobId);
     }
}
/**
 * Originially auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 * Modified last by Steve Reitz on 6/12/2020.
 **/
@IsTest(SeeAllData=true)
private class dlrs_AcctSeedERP_Purchase_OrderTest
{
    @IsTest(SeeAllData=true)
    private static void testTrigger()
    {
      // Force the dlrs_AcctSeedERP_Purchase_OrderTrigger to be invoked, fails the test if org config or other Apex code prevents this.
  
      //Used TestUtils so that it can insert Test Data in a clean code manner.
        Test.startTest();
        dlrs.RollupService.testHandler(TestUtils.createPurchaseOrder());
        Test.stopTest();
        
        
    }
}
global class CatalogProductUpdateBatchable implements Database.Batchable<Product2>,Database.AllowsCallouts {
    

    global List<Product2> product2UpdateBatch;
    global List<Product2> failedProduct2Syncs;
    public String store;
    public Decimal msrp;
    public Decimal price;


    public CatalogProductUpdateBatchable(List<Product2> product2UpdateBatch) {
        this.product2UpdateBatch=product2UpdateBatch;
    }
    global System.Iterable<Product2> start(Database.BatchableContext context){
        return product2UpdateBatch;
    }

    // global Database.QueryLocator start(Database.BatchableContext context) {
    //     String query='SELECT Id, tnw_mage_basic__Magento_ID__c,Website_IDs__c,Price_CAN__c,Manufacturer_Suggested_Retail_Price_CAN__c,Price__c,Manufacturers_Suggested_Retail_Price__c FROM Product2 WHERE tnw_mage_basic__Magento_ID__c!=NULL';
    //     return Database.getQueryLocator(query);
    // }

    global void execute(Database.BatchableContext context, List<Product2> product2UpdateBatch) {
                magentoSOAP.Port m = new magentoSOAP.Port();
                String sessionId = MagentoCalls.fetchSessionID();
                magentoSOAP.catalogProductCreateEntity productData = new magentoSOAP.catalogProductCreateEntity();
                List<Product2> retryProducts=new List<Product2>();
                List<Product2> failedProduct2Syncs=new List<Product2>();

                for(Product2 prod: product2UpdateBatch){  

                    String productId = prod.tnw_mage_basic__Magento_ID__c;
                    System.debug(productId);
                    String identifierType = 'id';//product.IdentifierType;
                    String prodWebIds=prod.Website_IDs__c;
                    System.debug(prodWebIds);

                    if (prodWebIds.contains('5')) {
                        store='firstbook_can';
                        price=prod.Price_CAN__c;
                        msrp=prod.Manufacturer_Suggested_Retail_Price_CAN__c;
                        String msrpString;
                                
                        
                        if (msrp!=null) {
                            msrpString= String.valueof(msrp.setScale(2));
                            
                        }

                        
                        if (price!=null){
                            productData.price = String.valueof(price.setScale(2));
                        }
                        
                       
                        try{
                            if(Test.isRunningTest()){
                                Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogProductUpdateFail'));
                            }
                            Boolean productUpdateResponse = m.catalogProductUpdate(sessionId,productId,productData,store,identifierType);
                                if(Test.isRunningTest()){
                                    Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('msrpFail'));
                                }
                                Integer msrpResponse = m.catalogProductSetSingleData(sessionId,productId,'msrp',msrpString,store);
                        }
                        catch(CalloutException ce){
                            CalloutException e = new CalloutException();
                            e.setMessage(productId+' '+ce);
                            if (ce.getMessage().contains('Read timed out')) {
                                retryProducts.add(prod);                           
                            }
                            //throw e;
                        }
                    }
                    if (prodWebIds.contains('4')) {
                        store='firstbook_us';
                        price=prod.Price__c;
                        msrp=prod.Manufacturers_Suggested_Retail_Price__c; 
                        String msrpString;       
                    
                    
                        if (msrp!=null) {
                            msrpString= String.valueof(msrp.setScale(2));
                            
                        }



                        productData.price = String.valueof(price.setScale(2));

                        try{
                            if(Test.isRunningTest()){
                                Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogProductUpdateFail'));
                            }
                            Boolean productUpdateResponse = m.catalogProductUpdate(sessionId,productId,productData,store,identifierType);
                                if(Test.isRunningTest()){
                                    Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('msrp'));
                                }
                                Integer msrpResponse = m.catalogProductSetSingleData(sessionId,productId,'msrp',msrpString,store);
                        }
                        catch(CalloutException ce){
                            CalloutException e = new CalloutException();
                            e.setMessage(productId+' '+ce);
                            if (ce.getMessage().contains('Read timed out')) {
                                retryProducts.add(prod);                           
                            }
                            else{
                                prod.addError('Product Failed to Sync to Magento');
                            }

                            
                            //throw e;
                        }
                    }
                    else{
                        break;
                    }


             } 
             if (retryProducts.size()>0) {
                System.enqueueJob(new CatalogProductUpdateActions(retryProducts));
                
            }
            // if (failedProduct2Syncs.size()>0) {
            //     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //     String[] toAddresses = new List<String>();
            //     toAddresses.add('sreitz@firstbook.org');
            //     mail.setToAddresses(toAddresses);
            //     mail.setSubject('Magento Update Batch Failed for Product(s) ');
            //     mail.setPlainTextBody('The following Products failed to sync to Magento: '+failedProduct2Syncs.toString());
            //     Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
            //     Messaging.SingleEmailMessage[] emailMessages=new List<Messaging.SingleEmailMessage>();
            //     emailMessages.add(mail);  
            //     Messaging.sendEmail(emailMessages);
            // }
                
            
    }

    global void finish(Database.BatchableContext context) {


        /*

        AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,     JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id =:context.getJobId()];
        batchCounter++;
        system.debug(batchCounter);
        if (a.NumberOfErrors==a.TotalJobItems) {
            failureCounter++;
            /*
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Magento Update Batch Failed for a Product ');
            mail.setPlainTextBody(a.TotalJobItems+' records processed ' +
           'with '+ a.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });         
        }
        if(batchCounter==allProductsSize){
            /*
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Magento Update Batch Completed ');
            mail.setPlainTextBody('There were '+failureCounter+' number of Products that failed to update.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); */
        
    
    }
}
@isTest public class TestGAFetch {
    
    @istest public static void CallWebService(){               
        
        APIConfig__c apiConfig = new APIConfig__c();
        apiConfig.apikey__c = 'abc123';
        apiConfig.endpoint__c = 'http://test/com';
        insert apiConfig;
        
        Test.startTest();
        // Create the mock response based on a static resource
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GAJSONData');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        
        Test.setMock(HttpCalloutMock.class, mock);        
        
		GADataFetch mkd = new GADataFetch();
		mkd.fetchData(null,null); 
        Test.stopTest();
       
    }   
    
    @istest public static void CallWebService2(){               
        
        APIConfig__c apiConfig = new APIConfig__c();
        apiConfig.apikey__c = 'abc123';
        apiConfig.endpoint__c = 'http://test/com';
        insert apiConfig;
        
        Test.startTest();
        // Create the mock response based on a static resource
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GAJSONData');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        
        Test.setMock(HttpCalloutMock.class, mock);        
        
		GADataFetch mkd = new GADataFetch();
		mkd.fetchData('2018-07-01','2018-07-09'); 
        Test.stopTest();
       
    }
    
    @istest public static void CallScheduler(){               
        
        GADataFetchScheduler acBatch = new GADataFetchScheduler('Batch');
       
        database.executebatch(acBatch);        
       
    }    
}
global without sharing class CatalogProductUpdateActions implements Queueable {

    //global List<CatalogProductUpdateRequest> productVals;

    global List<Product2> productVals;


    // public CatalogProductUpdateActions(List<CatalogProductUpdateRequest> productVals) {
    //     this.productVals=productVals;
    // }
    public CatalogProductUpdateActions(List<Product2> productVals) {
        this.productVals=productVals;
    }
    public void execute (QueueableContext context){
        // List<CatalogProductUpdateRequest> productUpdateBatch = new List<CatalogProductUpdateRequest>();
        // Integer requestCounter=0;
        // Integer batchSize=5;
        // for(CatalogProductUpdateRequest request: productVals){     
        //    productUpdateBatch.add(request);
        //    //TODO: make batch size also invocable, && (!(requestCounter<(productVals.size()-Math.mod(productVals.size(),3
        //    if ((requestCounter+batchSize)>productVals.size()) {
            
                    if(!Test.isRunningTest()){
                    Database.executeBatch(new CatalogProductUpdateBatchable(productVals), 5);
                    }
                       
                   
        
                //    catch (Exception e) {
                //     if(!Test.isRunningTest()){
                //     System.enqueueJob(new secondJob(productVals));
                //     }
                       
                   
            
          
                
            
 
             
        //         requestCounter=requestCounter+productUpdateBatch.size();
        //         System.debug('Special request counter '+requestCounter);
        //         productUpdateBatch.clear();
        //    } 
        //    else if (productUpdateBatch.size() == batchSize ) {
        //         System.debug('productUpdateBatch is at 3: '+ productUpdateBatch);
        //         Database.executeBatch(new CatalogProductUpdateBatchable(productUpdateBatch,productVals.size()), 1);
        //         productUpdateBatch.clear();
        //         requestCounter=requestCounter+batchSize;
        //         System.debug('requestCounter '+requestCounter);
        //     }
            
        //}
    }

    // @InvocableMethod(label='Catalog Product Update' description='Sends Price, MSRP, and Store to Magento.')
    // public static void catalogProductUpdate(List<CatalogProductUpdateRequest> productVals)
    // {
    //   	System.enqueueJob(new CatalogProductUpdateActions(productVals));
    // }


    @InvocableMethod(label='Catalog Product2 Update' description='Sends Price, MSRP, and Store to Magento.')
    public static void catalogProductUpdate(List<Product2> productVals)
    {



      	System.enqueueJob(new CatalogProductUpdateActions(productVals));
    }

    global class CatalogProductUpdateRequest {

        @InvocableVariable(label='Product Id' required=true)
        global String ProductId;
        // @InvocableVariable(label='Store' required=false)
        // global String store;

//        @InvocableVariable(label='Identifier Type' required=true)
//        global String IdentifierType;
        // @InvocableVariable(label='Price' required=true)
        // global Decimal price;
        // @InvocableVariable(label='MSRP' required=true)
        // global Decimal MSRP;
    }

}
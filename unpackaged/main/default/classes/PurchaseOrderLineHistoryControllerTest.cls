@isTest(seeAllData=TRUE)
public without sharing class PurchaseOrderLineHistoryControllerTest {

    @isTest
    public static void testGetPoPolHistory(){

        AcctSeedERP__Purchase_Order__c po = TestUtils.createPurchaseOrder();
        insert po;

        String glav1=TestUtils.createAccountingVariable('1').id;
        String glav2=TestUtils.createAccountingVariable('2').id;
        String glav3=TestUtils.createAccountingVariable('3').id;
        String poid= po.id;

        AcctSeed__GL_Account__c glAccountR= TestUtils.createGLAccount('Test glAccount','Revenue',True);
        AcctSeed__GL_Account__c glAccountE= TestUtils.createGLAccount('Test glAccount','Expense',True);
        AcctSeed__GL_Account__c glAccountI= TestUtils.createGLAccount('Test glAccount','Balance Sheet',True);
        Product2 prod= TestUtils.createProduct(glAccountR.id,glAccountE.id,glAccountI.id,TRUE);
        String prodid= prod.id;

        AcctSeedERP__Purchase_Order_Line__c pol=TestUtils.createPOLine(poid,prodid,5,glav2,glav3);
        insert pol;


        List<AcctSeedERP__Purchase_Order_Line__History> polhs= PurchaseOrderLineHistoryController.getPoPolHistory(po.id);
        System.assertEquals(0, polhs.size());
    }
}
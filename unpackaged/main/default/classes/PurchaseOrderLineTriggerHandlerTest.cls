@isTest
public without sharing class PurchaseOrderLineTriggerHandlerTest {
    public PurchaseOrderLineTriggerHandlerTest() {

    }

    
    @isTest(SeeAllData=true)
    public static void changeVerisonStatusDelete(){
                Test.startTest();
                
                // Create the PO
                AcctSeedERP__Purchase_Order__c po = TestUtils.createPurchaseOrder();
                po.Version__c='Original';
                insert po;

                // Create the Product
                AcctSeed__GL_Account__c glAccountR= TestUtils.createGLAccount('Test glAccount','Revenue',True);
                AcctSeed__GL_Account__c glAccountE= TestUtils.createGLAccount('Test glAccount','Expense',True);
                AcctSeed__GL_Account__c glAccountI= TestUtils.createGLAccount('Test glAccount','Balance Sheet',True);
                Product2 prod= TestUtils.createProduct(glAccountR.id,glAccountE.id,glAccountI.id);

                List<AcctSeedERP__Purchase_Order_Line__c> polList = new List<AcctSeedERP__Purchase_Order_Line__c>();
                // Create the PO Line(s)
                String glav2=TestUtils.createAccountingVariable('2').id;
                String glav3=TestUtils.createAccountingVariable('3').id;
                String poid= po.id;
                String prodid= prod.id;
                String whid=TestUtils.createWarehouse().id;
                String locid=TestUtils.createLocation(whid).id;
                for (Integer i = 0; i < 100; i++) {
                        AcctSeedERP__Purchase_Order_Line__c pol=TestUtils.createPOLine(poid,prodid,5,glav2,glav3);
                        pol.Warehouse__c=whid;
                        pol.Location__c=locid;
                        polList.add(pol);  
                }
                insert polList;

                po.Receiving_Warehouse__c=whid;
                po.AcctSeedERP__Status__c='Sent';
                // if virtual warehouse:
                // po.Receiving_Warehouse__c='a403I0000004YDEQA2';
                
                 
                 update po;
                 System.debug(po);

                //Update the POLs
                
                /*
                for (AcctSeedERP__Purchase_Order_Line__c pol:polList) {
                        pol.AcctSeedERP__Quantity__c=2;
                }
                update polList;*/
                delete polList[0];
                System.debug(polList);
                
                System.assertEquals('Sent',po.AcctSeedERP__Status__c);
                //System.assertEquals('Revision', po.Version__c);
                
                Test.stopTest();

    }

    @isTest(SeeAllData=true)
    public static void changeVerisonStatusUpdate(){
                Test.startTest();
                
                // Create the PO
                AcctSeedERP__Purchase_Order__c po = TestUtils.createPurchaseOrder();
                po.Version__c='Original';
                po.AcctSeedERP__Status__c='Sent';
                insert po;

                // Create the Product
                AcctSeed__GL_Account__c glAccountR= TestUtils.createGLAccount('Test glAccount','Revenue',True);
                AcctSeed__GL_Account__c glAccountE= TestUtils.createGLAccount('Test glAccount','Expense',True);
                AcctSeed__GL_Account__c glAccountI= TestUtils.createGLAccount('Test glAccount','Balance Sheet',True);
                Product2 prod= TestUtils.createProduct(glAccountR.id,glAccountE.id,glAccountI.id);

                List<AcctSeedERP__Purchase_Order_Line__c> polList = new List<AcctSeedERP__Purchase_Order_Line__c>();
                // Create the PO Line(s)
                String glav2=TestUtils.createAccountingVariable('2').id;
                String glav3=TestUtils.createAccountingVariable('3').id;
                String poid= po.id;
                String prodid= prod.id;
                String whid=TestUtils.createWarehouse().id;
                String locid=TestUtils.createLocation(whid).id;
                for (Integer i = 0; i < 100; i++) {
                        AcctSeedERP__Purchase_Order_Line__c pol=TestUtils.createPOLine(poid,prodid,5,glav2,glav3);
                        pol.Warehouse__c=whid;
                        pol.Location__c=locid;
                        polList.add(pol);  
                }
                insert polList;

                po.Receiving_Warehouse__c=whid;
                
                // if virtual warehouse:
                // po.Receiving_Warehouse__c='a403I0000004YDEQA2';
                
                 
                 update po;
                 System.debug(po);

                //Update the POLs
                
                
                for (AcctSeedERP__Purchase_Order_Line__c pol:polList) {
                        pol.AcctSeedERP__Quantity__c=2;
                        pol.AcctSeedERP__Unit_Price__c=2;
                        pol.Discount__c=2;
                        pol.Reorder_Carton_Quantity__c=2;
                        pol.Retail_Price__c=2;
                }
                update polList;

                System.debug(polList);
                
                System.assertEquals('Sent',po.AcctSeedERP__Status__c);
                //System.assertEquals('Revision', po.Version__c);
                
                Test.stopTest();

    }
    @isTest(SeeAllData=true)
    public static void setVerisonStatus(){
                Test.startTest();
                
                // Create the PO
                AcctSeedERP__Purchase_Order__c po = TestUtils.createPurchaseOrder();
                po.Version__c='Original';
                po.AcctSeedERP__Status__c='Sent';
                insert po;

                // Create the Product
                AcctSeed__GL_Account__c glAccountR= TestUtils.createGLAccount('Test glAccount','Revenue',True);
                AcctSeed__GL_Account__c glAccountE= TestUtils.createGLAccount('Test glAccount','Expense',True);
                AcctSeed__GL_Account__c glAccountI= TestUtils.createGLAccount('Test glAccount','Balance Sheet',True);
                Product2 prod= TestUtils.createProduct(glAccountR.id,glAccountE.id,glAccountI.id);

                List<AcctSeedERP__Purchase_Order_Line__c> polList = new List<AcctSeedERP__Purchase_Order_Line__c>();
                // Create the PO Line(s)
                String glav2=TestUtils.createAccountingVariable('2').id;
                String glav3=TestUtils.createAccountingVariable('3').id;
                String poid= po.id;
                String prodid= prod.id;
                String whid=TestUtils.createWarehouse().id;
                String locid=TestUtils.createLocation(whid).id;
                for (Integer i = 0; i < 100; i++) {
                        AcctSeedERP__Purchase_Order_Line__c pol=TestUtils.createPOLine(poid,prodid,5,glav2,glav3);
                        pol.Warehouse__c=whid;
                        pol.Location__c=locid;
                        polList.add(pol);  
                }
                insert polList;

                
                System.assertEquals('Sent',po.AcctSeedERP__Status__c);
                //System.assertEquals('Revision', po.Version__c);
                
                Test.stopTest();

    }

}
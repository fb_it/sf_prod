@isTest
public without sharing class ProductTriggerTest {

    @isTest
    public static void createProduct(){
        AcctSeed__GL_Account__c glAccountR= TestUtils.createGLAccount('Test glAccount','Revenue',True);
        AcctSeed__GL_Account__c glAccountE= TestUtils.createGLAccount('Test glAccount','Expense',True);
        AcctSeed__GL_Account__c glAccountI= TestUtils.createGLAccount('Test glAccount','Balance Sheet',True);
        Product2 prod= TestUtils.createProduct(glAccountR.id,glAccountE.id,glAccountI.id);
        prod.Qty_on_Order__c=3;
        update prod;
        try {
            delete prod;
            //throw new Exception('An exception should have been thrown by the trigger but was not.');
        } catch (Exception e) {
            Boolean expectedExceptionThrown;
            if (e.getMessage().contains('APEX ERROR')) {
               expectedExceptionThrown=  true;
                
            } 
            else {
                expectedExceptionThrown=false;
            }

            System.AssertEquals(expectedExceptionThrown, true);
            
        }
        

    }
}
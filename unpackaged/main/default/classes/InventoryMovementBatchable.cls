public without sharing class InventoryMovementBatchable implements Database.Batchable<sObject>,Database.AllowsCallouts {
    
    private List<SObject> scope;
    private String sessionId = 'testtoken';
    Decimal quantity;
    String magentoProductId;
    String warehouse;
    Boolean isDelete;
    String orderType;
    Map<Id,Decimal> oimProductIds= new Map<Id,Decimal>();


    public InventoryMovementBatchable(List<SObject> scope, String orderType,Boolean isDelete) {
        this.scope=scope;
        this.isDelete=isDelete;
        this.orderType=orderType;
    }


    public Iterable<SObject> start(Database.BatchableContext bc)
     {
        return scope;
     }

     private String getWarehouse(String warehouseName){
        String warehouseId;
        if(warehouseName == 'Maple Logistics')
        {
            warehouseId = '10';
        }
        else if(warehouseName  == 'American Book Company')
        {
            warehouseId = '8';
        }
        else if(warehouseName  == 'Virtual Warehouse')
        {
            warehouseId = '17';
        }
        return warehouseId;
     }

     private List<Product2> updateMovedProducts(Map<Id,Decimal> oimProductIds)
     {
        List<Product2> poProdList=new List<Product2>();
        Set<Id> poimProductIdSet = oimProductIds.keySet();
        List<Product2> poProdSetList= [SELECT Id, Catalog_Available_Qty__c FROM Product2 WHERE Id IN :poimProductIdSet];
        for (Product2 poProd: poProdSetList) {
            quantity=oimProductIds.get(poProd.id);
            poProd.Catalog_Available_Qty__c=poProd.Catalog_Available_Qty__c+quantity;
            poProdList.add(poProd);
            
        }
        return poProdList;
     }

     public void execute(Database.BatchableContext BC, List<SObject> listName)
     {
        magentoSOAP.Port m = new magentoSOAP.Port();
        if(Test.isRunningTest()){
            Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('loginFail'));
        }
        try {
            sessionId = MagentoCalls.fetchSessionID();
            
        } catch (CalloutException ce) {
            if (ce.getMessage().contains('Read timed out')) {
                for (SObject so : listName) {
                    so.addError('URGENT ERROR: Check on the status of Magento. ');
                    
                }
                
            }

            
        }
        


        System.debug('listname size '+listName.size());
        System.debug('listname '+listName);
        if (orderType=='Purchase Order') {
            List<AcctSeedERP__Purchase_Order_Inventory_Movement__c> retryPOIMs=new List<AcctSeedERP__Purchase_Order_Inventory_Movement__c>();    
            for (SObject so: listName) {
                AcctSeedERP__Purchase_Order_Inventory_Movement__c s=(AcctSeedERP__Purchase_Order_Inventory_Movement__c)so;
                System.debug(s);
                if(isDelete){
                    quantity=-1*s.AcctSeedERP__Quantity__c;
                }
                else{
                    quantity=s.AcctSeedERP__Quantity__c;
                }
                system.debug(quantity);
                warehouse=getWarehouse(s.AcctSeedERP__Warehouse__c);
                magentoProductId=s.Product_Magento_Id__c;
                system.debug(magentoProductId);

                try{
                    if(Test.isRunningTest()){
                        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogInventoryStockItemUpdateDelta'));
                    }
                    Integer response=m.catalogInventoryStockItemUpdateDelta(sessionId,magentoProductId,(Integer)quantity);
                    

                    if(Test.isRunningTest()){
                        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogInventoryStockItemUpdateErp'));
                    }
                    Integer erpResponse = m.catalogInventoryStockItemUpdateErp(sessionId,warehouse,magentoProductId,String.valueOf((Integer)quantity));
                }
                catch(CalloutException ce){
                    if (ce.getMessage().contains('Read timed out')) {
                        retryPOIMs.add(s);
                    }
                }


                if(quantity>0)
                {
                    if (oimProductIds.containsKey(s.AcctSeedERP__ProductId__c)) {
                        Decimal quantityToAdd= oimProductIds.get(s.AcctSeedERP__ProductId__c);
                        quantity= quantity+quantityToAdd;
                        oimProductIds.put(s.AcctSeedERP__ProductId__c,quantity);  
                    }
                    else {
                        oimProductIds.put(s.AcctSeedERP__ProductId__c,quantity);               
                    }

                }
            }


            List<Product2> poProdList=updateMovedProducts(oimProductIds);
            if (retryPOIMs.size()>0) {
                if(!Test.isRunningTest()){
                     ID jobId=Database.executeBatch(new InventoryMovementBatchable(retryPOIMs,'Purchase Order',isDelete),45);    
                }
               
            }
            if(poProdList.size()>0){
                Database.update(poProdList);
           }

        }

        
            



     }

     public void finish(Database.BatchableContext BC)
     {

     }


}
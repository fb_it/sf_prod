public without sharing class PurchaseOrderLineHistoryController {
    @AuraEnabled(cacheable=true)
    public static List<AcctSeedERP__Purchase_Order_Line__History> getPoPolHistory(String recordId) {
        List<AcctSeedERP__Purchase_Order_Line__History> correctedQuery= new List<AcctSeedERP__Purchase_Order_Line__History>();
        List<AcctSeedERP__Purchase_Order_Line__History> query= [
            SELECT  
            Parent.Name,
            Parent.AcctSeedERP__Product__r.Name,
            Parent.AcctSeedERP__Product__r.StockKeepingUnit,
            Field,
            OldValue,
            NewValue,
            CreatedDate, 
            CreatedBy.Alias
            FROM AcctSeedERP__Purchase_Order_Line__History 
            WHERE Parent.AcctSeedERP__Purchase_Order__c=:recordId 
            ORDER BY CreatedDate DESC
        ];
        for (AcctSeedERP__Purchase_Order_Line__History polh : query) {
            if(polh.OldValue==null && polh.NewValue==null && polh.field!='created' ){
                System.debug(polh);
            }
            else{
                correctedQuery.add(polh);
            }
            

            
        }

        return correctedQuery;
    }
}
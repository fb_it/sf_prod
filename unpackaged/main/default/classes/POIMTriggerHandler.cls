public without sharing class POIMTriggerHandler {

    @future(Callout=true)
    public static void SendToMagento(String newListJSON, Boolean isDelete)
    {
        List<AcctSeedERP__Purchase_Order_Inventory_Movement__c> newList = (List<AcctSeedERP__Purchase_Order_Inventory_Movement__c>)JSON.deserialize(newListJSON,List<AcctSeedERP__Purchase_Order_Inventory_Movement__c>.class);
        System.debug(newList);
        String q ='Select id, Username__c, API_Key__c from Magento_Setting__mdt ';
        if(runningInASandbox)
        {
            q += 'WHERE DeveloperName = \'Sandbox\' limit 1';
        }
        else
        {
            q += 'WHERE DeveloperName = \'Production\' limit 1';
        }

        magentoSOAP.Port m = new magentoSOAP.Port();
        String sessionId = 'testtoken';
        if(Test.isRunningTest()){
            Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('login'));
        }
        sessionId = MagentoCalls.fetchSessionID();

        system.debug(sessionId);

        List<AcctSeedERP__Purchase_Order_Inventory_Movement__c> poimList= [Select id, AcctSeedERP__Warehouse__c,AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Product__r.tnw_mage_basic__Magento_ID__c, AcctSeedERP__Quantity__c, AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Location__r.Magento_Sync__c from AcctSeedERP__Purchase_Order_Inventory_Movement__c where ID in: newList AND AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Location__r.Magento_Sync__c = TRUE AND AcctSeedERP__Quantity__c >0 ALL ROWS];

        List<AcctSeedERP__Purchase_Order_Inventory_Movement__c> poimUpdateList= new List<AcctSeedERP__Purchase_Order_Inventory_Movement__c>();

        for(AcctSeedERP__Purchase_Order_Inventory_Movement__c poim:poimList)
        {
            Decimal quantity = isDelete ? poim.AcctSeedERP__Quantity__c  * -1: poim.AcctSeedERP__Quantity__c;
            system.debug('Quantity -- ' + String.valueof((Integer)quantity));
            poim.catalogInvStockItemUpdateDelta_quantity__c=quantity;
            if(Test.isRunningTest()){
                Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogInventoryStockItemUpdateDelta'));
            }
            Integer response=m.catalogInventoryStockItemUpdateDelta(sessionId,poim.AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Product__r.tnw_mage_basic__Magento_ID__c,(Integer)quantity);
            //poim.catalogInvStockItemUpdateDelta_response__c=response;
            if (response==1) {
                poim.catalogInvStockItemUpdateDelta_response__c=TRUE;                 
            }

            String warehouse;
            if(poim.AcctSeedERP__Warehouse__c == 'Maple Logistics')
            {
                warehouse = '10';
            }
            else if(poim.AcctSeedERP__Warehouse__c == 'American Book Company')
            {
                warehouse = '8';
            }
            else if(poim.AcctSeedERP__Warehouse__c == 'Virtual Warehouse')
            {
                warehouse = '17';
            }
            quantity=(Integer)quantity;
            system.debug('Quantity -- ' + String.valueof(quantity));
            poim.catalogInvStockItemUpdateERP_quantity__c=quantity;
            if(Test.isRunningTest()){
                Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogInventoryStockItemUpdateErp'));
            }
            Integer erpResponse = m.catalogInventoryStockItemUpdateErp(sessionId,warehouse,poim.AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Product__r.tnw_mage_basic__Magento_ID__c,String.valueOf(quantity));
            
            if (erpResponse==1) {
                poim.catalogInvStockItemUpdateERP_response__c=TRUE; 
            }
            
            //poim.catalogInvStockItemUpdateERP_response__c=erpResponse;
            //System.debug('poim ERP response '+poim.catalogInvStockItemUpdateERP_response__c);
            //system.debug('response = ' + response);
            if(!isDelete){
                poimUpdateList.add(poim);
            }
            
        }
        System.debug(poimList);
        if(poimUpdateList.size()>0){
            update poimUpdateList;
        }
        


    }

    public static Boolean runningInASandbox {
        get {
            if (runningInASandbox == null) {
                runningInASandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
            }
            return runningInASandbox;
        }
        set;
    }
}
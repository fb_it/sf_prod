global class GADataFetchJob implements Schedulable {
    
   global void execute(SchedulableContext SC) {       
        try 
        {
            // Abort the job you have just scheduled via Setup 
            CronTrigger ct = [SELECT id,CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :SC.getTriggerId()];
            if(ct != null)
            {
                System.abortJob(ct.Id);
            }
        }
        catch(Exception e) 
        {
            System.debug('No jobs scheduled ' + e.getMessage()); 
        }        
       
        GADataFetchScheduler acBatch = new GADataFetchScheduler('Batch');
       
        database.executebatch(acBatch,50);       
   }    
}
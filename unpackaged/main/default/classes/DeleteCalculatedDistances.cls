global without sharing class DeleteCalculatedDistances implements Database.Batchable<SObject>{
    
    private String campId;
    private String source;
    
    private List<Calculated_Distance__c> calculatedDistanceList = new List<Calculated_Distance__c>();
    
    global DeleteCalculatedDistances(String cmpId, String src){
    	campId = cmpId;
        source = src;
    }
        
    global Database.QueryLocator start(Database.BatchableContext BC) { 
        System.debug('DeleteCalculatedDistances: starting batch for deleting existing mappings for campId:' + campId);
                
        String query = 'Select ID from Calculated_Distance__c where Campaign__c = :campId';

        return Database.getQueryLocator(query);        
    }
    
    global void execute(Database.BatchableContext BC, List<Calculated_Distance__c> calculatedDistanceList){
        
        if(!calculatedDistanceList.isEmpty()){
            delete calculatedDistanceList;
            System.debug('Existing mappings deleted for campaign');
        }
        
    }
    
    global void finish(Database.BatchableContext BC){ 
   		CalculateDistancesController cntrl = new CalculateDistancesController();
        cntrl.deleteCompleted(source, campId);
    }    
}
public class GADataFetch {
    
    List<Opportunity> opportunityList = new List<Opportunity>();
    
    public GADataFetch(){
        //endPoint = 'https://pacific-depths-15722.herokuapp.com/api/views/firstbook';
    }
    
    
    private void setAPIConfigForNextCall(Long starttime, Long endtime){
        
        APIConfig__c apiConfig = [select Id,starttime__c, endtime__c from APIConfig__c];
        
        System.debug('apiConfig:'+apiConfig);
        
        //apiConfig.starttime__c = null;
        //apiConfig.endtime__c = null;
		DateTime estTime = DateTime.newInstance(endtime*1000);
        
        //Adding 8 hrs bcoz the batch runs every 8 hrs
        estTime = estTime.addHours(8);
        
        //Go back 2 days
        apiConfig.starttime__c = estTime.addDays(-2);
        apiConfig.endtime__c = estTime;
        
        System.debug('New apiConfig:'+apiConfig);
        
        try{
            Database.update(apiConfig, false);
	       // update apiConfig;
            apiConfig = [select Id,starttime__c, endtime__c from APIConfig__c];
            System.debug('API Config Updated : ' + apiConfig);
        }Catch(DMLException e){
            System.debug('Exception : ' + e.getMessage());
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));        	    
        }
		
    }
    
    private String getEndpointForThisCall(String dataType, String startDate, String endDate){
        
        String endPoint;
        
        APIConfig__c apiConfig = [select Id,apikey__c,endpoint__c,starttime__c, endtime__c from APIConfig__c];
        
        endPoint = apiConfig.endpoint__c;

        if(startDate != null && endDate != null){
            //The specified string should use the standard date format “yyyy-MM-dd HH:mm:ss” in the local time zone.        
            Datetime startDateTime = Datetime.valueOf(startDate + ' 00:00:00');
            Datetime endDateTime = Datetime.valueOf(endDate + ' 00:00:00');        
    
            //The returned date is in the GMT time zone so convert it into EST.        
            startDateTime.addHours(-5);
            endDateTime.addHours(-5);
            
            endPoint = endPoint + '/'+ dataType + '/' + dateTimeToSecs(startDateTime);
            endPoint = endPoint + '/' + dateTimeToSecs(endDateTime);
        }else{
            if(apiConfig.starttime__c != null && apiConfig.endtime__c != null){
                endPoint = endPoint + '/'+ dataType + '/' + dateTimeToSecs(apiConfig.starttime__c);
                endPoint = endPoint + '/' + dateTimeToSecs(apiConfig.endtime__c);
            }else{
                //Now() Returns the current Datetime based on a GMT calendar. Convert to EST
                DateTime nowTimeEST = (DateTime.now()).addHours(-5);
                apiConfig.starttime__c = nowTimeEST.addDays(-2);
                //Go back 2 days
                apiConfig.endtime__c = nowTimeEST;
                
                endPoint = endPoint + '/'+ dataType + '/' + dateTimeToSecs(apiConfig.starttime__c);
                endPoint = endPoint + '/' + dateTimeToSecs(apiConfig.endtime__c);                
            }            
        }
       
        endPoint = endPoint + '/' + apiConfig.apikey__c;
        
        return endPoint;
    }
    
    private Long dateTimeToSecs(DateTime dataIn){
        Long dataOut;
        Decimal datetimeNowDec = ((DateTime.now()).getTime())/1000;
        Long datetimeNow = datetimeNowDec.round(System.RoundingMode.DOWN);
        
        Decimal dataDec = (dataIn.getTime())/1000;
        dataOut = dataDec.round(System.RoundingMode.DOWN);
        dataOut = dataOut < datetimeNow ? dataOut : datetimeNow;
        return dataOut;
    }
    
    public void fetchDataViaVF(){
    	//fetchData(null,null);    
        GADataFetchScheduler acBatch = new GADataFetchScheduler('vfpage');       
        database.executebatch(acBatch,50);  
    }
    
    public List<Opportunity> fetchData(String startDate, String endDate){
        try{
            //This is for the GA Property:  UA-330996-1
            String endPoint1 = getEndpointForThisCall('firstbook',startDate,endDate);  
            //This is for the GA Property:  UA330996-14
            String endPoint2 = getEndpointForThisCall('firstbook-ca',startDate,endDate);
            
            Map<String, Object> results1 = fetchGAData(endPoint1,'magento');        
            Map<String, Object> results2 = fetchGAData(endPoint2,'classy');  
    
            Long starttime;
            Long endtime;
            
            if(results1 != null){
                if(results1.get('transactions') instanceof Map<String, Object>){
                    Map<String, Object> transactions = (Map<String, Object>)results1.get('transactions');  
                    if(transactions != null){
                        system.debug('magento transactions:');
                        insertGAData(transactions,'magento');
                        insertGAData(transactions,'classy');
                    }                
                }  
                starttime = Long.valueOf((String)results1.get('starttime'));
                endtime = Long.valueOf((String)results1.get('endtime'));            
            } 
            if(results2 != null){
                if(results2.get('transactions') instanceof Map<String, Object>){
                    Map<String, Object> transactions = (Map<String, Object>)results2.get('transactions');  
                    if(transactions != null){
                        system.debug('classy transactions:');
                        insertGAData(transactions,'classy');
                    }                
                }  
                starttime = Long.valueOf((String)results2.get('starttime'));
                endtime = Long.valueOf((String)results2.get('endtime'));             
            }  
           
            setAPIConfigForNextCall(starttime, endtime);            
        }Catch(Exception e){
            System.debug('Exception : ' + e.getMessage());
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));    
        }    
        
        return opportunityList;
    }
    
    public Map<String, Object> fetchGAData(String endpoint, String dataType){
		String jsonData = '{"userId-327565":{"ga:source":"(direct)","ga:campaign":"(not set)","ga:medium":"(none)","ga:landingPagePath":"/?page=join&pkg=0&cntry=US","ga:transactionId":"userId-327565","ga:keyword":"(not set)"}}';
        
        Map<String, Object> results;
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        system.debug('The endpoint is:'+endpoint);
        
        request.setEndpoint(endPoint);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        
        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            // Deserialize the JSON string into collections of primitive data types.
            results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            //Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(jsonData);                            			                                             
        }
        else{
            System.debug('API Failed:'+response); 
        }
        
        return results;
    }   
    
    private void insertGAData(Map<String, Object> transactions, String dataType){                             
        Set <String> transactionKeys = transactions.keySet();
		Map<String, Opportunity> existingOpps = new Map<String, Opportunity>();
        
        Opportunity[] opps;
        
        System.debug('transactionKeys:'+transactionKeys);
        
        if(dataType == 'classy'){
            opps = [Select Id, stayclassy__sc_order_id__c from Opportunity where stayclassy__sc_order_id__c in :transactionKeys];
            for(Opportunity opp:opps){
                existingOpps.put(opp.stayclassy__sc_order_id__c, opp);
            }            
        }else{
            opps = [Select Id, tnw_mage_basic__Magento_ID__c from Opportunity where tnw_mage_basic__Magento_ID__c in :transactionKeys];
            for(Opportunity opp:opps){
                existingOpps.put(opp.tnw_mage_basic__Magento_ID__c, opp);
            }             
        }
 
        for (String trans: transactionKeys) {
            Map<String, Object> transMap = (Map<String, Object>)transactions.get(trans);
            
            //System.debug('Transaction:'+transMap); 
            
            if(trans == null || String.isBlank(trans)){
                continue;
            }
            //insert into SF
            //{ga:campaign=(not set), ga:keyword=(not set), ga:landingPagePath=support.firstbook.org/campaign/essentials-for-kids-fund/c138937, ga:medium=(none), ga:source=(direct), ga:transactionId=8572495}
            if(existingOpps.containsKey(trans)){
                Opportunity oppt = (Opportunity)existingOpps.get(trans);
                
                oppt.Google_Analytics_Link_URL__c = (String)transMap.get('ga:landingPagePath');
                oppt.Google_Analytics_UTM_Campaign__c = (String)transMap.get('ga:campaign');
                oppt.Google_Analytics_UTM_Medium__c = (String)transMap.get('ga:medium');
                oppt.Google_Analytics_UTM_Source__c = (String)transMap.get('ga:source');
                oppt.Google_Analytics_UTM_Term__c = (String)transMap.get('ga:keyword');
                oppt.Google_Analytics_UTM_Content__c = (String)transMap.get('ga:adContent');
               
                opportunityList.add(oppt);               
            }               
        }
        
        System.debug('OpportunityList to Update:'+opportunityList);
		        
        /*
        if(!opportunityList.isEmpty()){
            try{
                Database.update(opportunityList, false);
                System.debug('After OpportunityList Update.');
            }catch (DMLException e){
                System.debug('Exception:'+e.getMessage());
            }            
        }
*/
    } 
}
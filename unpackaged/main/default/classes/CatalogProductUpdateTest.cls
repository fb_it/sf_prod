@isTest
private class CatalogProductUpdateTest {

    @isTest
    private static void testCatalogProductUpdate(){
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('login'));
        
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogProductUpdate'));

        Product2 prod = new Product2();
        List<Product2> prodList2= new List<Product2>();

        for(Integer i=0; i<5;i++){
            AcctSeed__GL_Account__c glAccountR= TestUtils.createGLAccount('Test glAccount','Revenue',True);
            AcctSeed__GL_Account__c glAccountE= TestUtils.createGLAccount('Test glAccount','Expense',True);
            AcctSeed__GL_Account__c glAccountI= TestUtils.createGLAccount('Test glAccount','Balance Sheet',True);
            Product2 prod2= TestUtils.createProduct(glAccountR.id,glAccountE.id,glAccountI.id);
            insert prod2;

            prod2.tnw_mage_basic__Magento_ID__c='3288'+i;
            prod2.Price__c = 12.34;
            prod2.Price_CAN__c = 12.34;
            prod2.Website_IDs__c='1,4,5';
            prod2.Manufacturers_Suggested_Retail_Price__c = 56.78;
            prod2.Manufacturer_Suggested_Retail_Price_CAN__c = 56.87;
            prodList2.add(prod2);
        }

        CatalogProductUpdateActions.catalogProductUpdate(prodList2);
        ID dataBatchId=Database.executeBatch(new CatalogProductUpdateBatchable(prodList2), 5);
        

    }
}
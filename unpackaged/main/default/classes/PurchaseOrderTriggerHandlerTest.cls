@isTest(SeeAllData=true)
public without sharing class PurchaseOrderTriggerHandlerTest {
    public PurchaseOrderTriggerHandlerTest() {

    }
    @isTest
    public static void testUtilsTest(){
                
                
                // Create the PO
                AcctSeedERP__Purchase_Order__c po = TestUtils.createPurchaseOrder();
                insert po;

                // Create the Product
                AcctSeed__GL_Account__c glAccountR= TestUtils.createGLAccount('Test glAccount','Revenue',True);
                AcctSeed__GL_Account__c glAccountE= TestUtils.createGLAccount('Test glAccount','Expense',True);
                AcctSeed__GL_Account__c glAccountI= TestUtils.createGLAccount('Test glAccount','Balance Sheet',True);
                Product2 prod= TestUtils.createProduct(glAccountR.id,glAccountE.id,glAccountI.id);

                List<AcctSeedERP__Purchase_Order_Line__c> polList = new List<AcctSeedERP__Purchase_Order_Line__c>();
                // Create the PO Line(s)
                String glav2=TestUtils.createAccountingVariable('2').id;
                String glav3=TestUtils.createAccountingVariable('3').id;
                String poid= po.id;
                String prodid= prod.id;
                AcctSeedERP__Inventory_Balance__c iqa=TestUtils.createIQA(prod);
                String iqaid=iqa.id;
                String wh=TestUtils.createWarehouse().id;
                for (Integer i = 0; i < 100; i++) {
                        AcctSeedERP__Purchase_Order_Line__c pol=TestUtils.createPOLine(poid,prodid,5,glav2,glav3);
                        //String whid=TestUtils.createWarehouse().id;
                        pol.Warehouse__c=wh;
                        pol.Location__c=TestUtils.createLocation(wh).id;
                        pol.Inventory_Balance__c=iqaid;
                        System.debug('pol '+pol.AcctSeedERP__Quantity_Unreceived__c);
                        polList.add(pol);  
                }
                insert polList;

                /*
                //Update the POLs
                
                for (AcctSeedERP__Purchase_Order_Line__c pol:polList) {
                        pol.AcctSeedERP__Unit_Price__c=2;
                }
                update polList;*/
                
                po.Receiving_Warehouse__c=wh;
               // if virtual warehouse:
               // po.Receiving_Warehouse__c='a403I0000004YDEQA2';
                po.AcctSeedERP__Status__c='Sent';
                update po;
                System.debug(prod.id+' '+prod.Qty_on_Order__c);
                

                delete po;

                System.debug(prod.id+' '+prod.Qty_on_Order__c+' 2');

               Integer count=[SELECT count() FROM AcctSeedERP__Purchase_Order__c WHERE Id=:po.id];

                System.assertEquals(0, count);
                


    }
}
@isTest
public class GAUAllocationTriggerTest {
    public static testmethod void testGAURollupAllocation() {
        
        RecordType rType;
        //RecordType rType = [SELECT id from RecordType where SobjectType='npsp__General_Accounting_Unit__c' and DeveloperName='Allocation'];
        
        
        npsp__General_Accounting_Unit__c gau = new npsp__General_Accounting_Unit__c();
        gau.Name = 'testgau';
        //gau.RecordTypeId = rType.id;
        insert gau;
        
        //Create opportunity
        rType = [SELECT id from RecordType where SobjectType='opportunity' and DeveloperName='Donation'];
        
        opportunity opp = new opportunity();
        opp.StageName = 'Posted';
        opp.CloseDate = date.today();
        opp.RecordTypeId = rType.Id;
        opp.Name = 'testopp';
        opp.Amount = 10.00;
        
        insert opp;
            
        rType = [SELECT id from RecordType where SobjectType='npsp__Allocation__c' and DeveloperName='Allocation'];
        
        npsp__Allocation__c alloc = new npsp__Allocation__c();
        alloc.npsp__General_Accounting_Unit__c = gau.id;
        alloc.npsp__Opportunity__c = opp.id;
        alloc.npsp__Amount__c = 5.00;
        alloc.RecordTypeId = rType.id;
        
        insert alloc;
        
        alloc.npsp__Amount__c = 7.00;
        
        update alloc;  
        delete alloc;
        
    }

    public static testmethod void testGAURollupDisbursement() {
        
        RecordType rType;
        //RecordType rType = [SELECT id from RecordType where SobjectType='npsp__General_Accounting_Unit__c' and DeveloperName='Allocation'];
            
        npsp__General_Accounting_Unit__c gau = new npsp__General_Accounting_Unit__c();
        gau.Name = 'testgau';
        Campaign camp = new Campaign();
        camp.Name = 'testcamp';
        //gau.RecordTypeId = rType.id;
        insert gau;
        insert camp;
        
        //Create opportunity
        rType = [SELECT id from RecordType where SobjectType='opportunity' and DeveloperName='Donation'];
        
        opportunity opp = new opportunity();
        opp.StageName = 'Posted';
        opp.CloseDate = date.today();
        opp.RecordTypeId = rType.Id;
        opp.Name = 'testopp';
        opp.Amount = 10.00;
        opp.Gift_Certificate_Amount__c = 4.00;
        opp.Gift_Certificate_Invoiced__c = 4.00;        
        
        insert opp;                    
               
        rType = [SELECT id from RecordType where SobjectType='npsp__Allocation__c' and DeveloperName='Disbursement'];
        
        npsp__Allocation__c disb = new npsp__Allocation__c();
        disb.npsp__General_Accounting_Unit__c = gau.id;
        disb.npsp__Campaign__c = camp.id;
        disb.npsp__Amount__c = 5.00;
        disb.Total_VGC_Amount_Spent__c = 2.00;
        disb.RecordTypeId = rType.id;
        
        insert disb;  
        
        disb.npsp__Amount__c = 7.00;
        disb.Total_VGC_Amount_Spent__c = 3.00;
        update disb;
        
        delete disb;
        
    }
    
    public static testmethod void testGAURollupAllocationNullAmount() {
        
        RecordType rType;
        //RecordType rType = [SELECT id from RecordType where SobjectType='npsp__General_Accounting_Unit__c' and DeveloperName='Allocation'];
            
        npsp__General_Accounting_Unit__c gau = new npsp__General_Accounting_Unit__c();
        gau.Name = 'testgau';
        //gau.RecordTypeId = rType.id;
        insert gau;
        
        //Create opportunity
        rType = [SELECT id from RecordType where SobjectType='opportunity' and DeveloperName='Donation'];
        
        opportunity opp = new opportunity();
        opp.StageName = 'Posted';
        opp.CloseDate = date.today();
        opp.RecordTypeId = rType.Id;
        opp.Name = 'testopp';
        opp.Amount = 10.00;
        
        insert opp;
            
        rType = [SELECT id from RecordType where SobjectType='npsp__Allocation__c' and DeveloperName='Allocation'];
        
        npsp__Allocation__c alloc = new npsp__Allocation__c();
        alloc.npsp__General_Accounting_Unit__c = gau.id;
        alloc.npsp__Opportunity__c = opp.id;
        alloc.RecordTypeId = rType.id;
        alloc.npsp__Percent__c = 50.00;
        
        insert alloc;
        
        alloc.npsp__Amount__c = 7.00;
        
        update alloc;  
        delete alloc;
        
    } 
    
    public static testmethod void testGAURollupAllocationPaid() {
        
        RecordType rType;
        //RecordType rType = [SELECT id from RecordType where SobjectType='npsp__General_Accounting_Unit__c' and DeveloperName='Allocation'];
            
        npsp__General_Accounting_Unit__c gau = new npsp__General_Accounting_Unit__c();
        gau.Name = 'testgau';
        //gau.RecordTypeId = rType.id;
        insert gau;
        
        //Create opportunity
        rType = [SELECT id from RecordType where SobjectType='opportunity' and DeveloperName='Donation'];
        
        opportunity opp = new opportunity();
        opp.StageName = 'Posted';
        opp.CloseDate = date.today();
        opp.RecordTypeId = rType.Id;
        opp.Name = 'testopp';
        opp.Amount = 10.00;
        
        insert opp;
            
        rType = [SELECT id from RecordType where SobjectType='npsp__Allocation__c' and DeveloperName='Allocation'];
        
        npsp__Allocation__c alloc = new npsp__Allocation__c();
        alloc.npsp__General_Accounting_Unit__c = gau.id;
        alloc.npsp__Opportunity__c = opp.id;
        alloc.npsp__Amount__c = 5.00;
        alloc.RecordTypeId = rType.id;
        alloc.Paid__c = True;
        
        insert alloc;
        
        alloc.npsp__Amount__c = 7.00;
        
        update alloc;  
        delete alloc;
        
    }    
}